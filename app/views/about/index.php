<div class="container my-5">
    <div class="row d-flex justify-content-center align-items-center">
        <div class="col-lg-6">
            <img src="<?= BASEURL ?>/img/about/about-img.svg" alt="" class="img-fluid" loading="lazy">
        </div>
        <div class="col-lg-6">
            <h1 class="fw-bold text-abu">
                A Wonderful Agency To Fulfill your Dreams
            </h1>
            <p class="text-abu my-3">
                The Hajj and Umrah pilgrimages are smooth, the heart is clean. we have been trusted since 2006 and have obtained a license from SAUDI ARABIA to become this travel agency.
            </p>
            <div class="side-right my-4 d-flex flex-column gap-3">
                <div class="d-flex align-items-center gap-3">
                    <img src="<?= BASEURL ?>/img/icons/plane-icon.svg" alt="">
                    <p class="text-abu mb-0">Flight Jakarta Saudi - Saudi Jakarta</p>
                </div>
                <div class="d-flex align-items-center gap-3">
                    <img src="<?= BASEURL ?>/img/icons/ticket-icon.svg" alt="">
                    <p class="text-abu mb-0">Tickets are included in the cost</p>
                </div>
                <div class="d-flex align-items-center gap-3">
                    <img src="<?= BASEURL ?>/img/icons/hostel-icon.svg" alt="">
                    <p class="text-abu mb-0">Comfortable hotel with a rating of 4</p>
                </div>
                <div class="d-flex align-items-center gap-3">
                    <img src="<?= BASEURL ?>/img/icons/visas-icon.svg" alt="">
                    <p class="text-abu mb-0">We cover the Visa Fee and it's Included</p>
                </div>
            </div>
            <div class="d-flex gap-3">
                <button class="btn bg-active px-4 py-3 ">
                    Learn more
                </button>
                <button class="btn px-4 py-3 text-active" style="border: 2px solid #FFC265;">
                    Contact us
                </button>
            </div>
        </div>
    </div>
</div>