<div class="container my-5">

    <div class="row d-flex justify-content-center">
        <div class="col-lg-8">
            <?= Flasher::flash(); ?>
        </div>
    </div>

    <div class="row d-flex justify-content-center">
        <div class="col-sm-8 col-lg-8 mg-t-10 mg-lg-t-0">
            <div class="card">
                <div class="card-header">
                    <h5 id="section2" class="mg-b-10 text-center">Create Blog</h5>
                </div>
                <div class="card-body">
                    <form action="<?= BASEURL; ?>/blog/tambah" method="post" enctype="multipart/form-data">
                        <div class="form-group mt-3">
                            <label for="title">Judul:</label>
                            <input type="text" class="form-control" name="judul" id="title" required>
                        </div>

                        <div class="form-group mt-3">
                            <input id="x" type="hidden" name="deskripsi">
                            <trix-editor input="x"></trix-editor>
                        </div>

                        <div class="form-group mt-3">
                            <label for="category">Kategori:</label>
                            <select class="form-select" name="kategori" id="category" required>
                                <option value="Pilih Kategori">Pilih Kategori</option>
                                <option value="Teknologi">Teknologi</option>
                                <option value="Olahraga">Olahraga</option>
                                <option value="Travel">Travel</option>
                                <option value="Lifestyle">Lifestyle</option>
                            </select>
                        </div>

                        <img id="previewImage" width="200">
                        <div class="form-group mt-3 w-100">
                            <label for="image">Gambar:</label>
                            <div id="drop-area">
                                <input type="file" id="image" accept="image/" class="opacity-0 w-100" name="gambar">
                                <p>Drag & Drop atau klik di sini untuk mengunggah gambar.</p>
                            </div>
                        </div>
                </div>
                <div class="card-footer">
                    <div class="text-end">
                        <button type="submit" class="btn bg-active text-white">
                            <i class="fa-solid fa-floppy-disk "></i> Save
                        </button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>


<style>
    #drop-area {
        border: 2px dashed #ccc;
        text-align: center;
        padding: 20px;
        cursor: pointer;
    }
</style>

<script>
    const dropArea = document.getElementById('drop-area');
    const imageInput = document.getElementById('image');
    const previewImage = document.getElementById('previewImage');

    imageInput.addEventListener('change', function() {
        const file = imageInput.files[0];
        if (file) {
            const reader = new FileReader();

            reader.onload = function(e) {
                previewImage.src = e.target.result;
            };

            reader.readAsDataURL(file);
        } else {
            previewImage.src = '';
        }
    })

    dropArea.addEventListener('dragover', (e) => {
        e.preventDefault();
        dropArea.style.border = '2px dashed #000';
    });

    dropArea.addEventListener('dragleave', () => {
        dropArea.style.border = '2px dashed #ccc';
    });

    dropArea.addEventListener('drop', (e) => {
        e.preventDefault();
        dropArea.style.border = '2px dashed #ccc';
        const file = e.dataTransfer.files[0];
        imageInput.files = e.dataTransfer.files;
    });

    dropArea.addEventListener('click', () => {
        imageInput.click();
    });
</script>