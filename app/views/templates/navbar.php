 <nav class="navbar navbar-expand-lg">
     <div class="container d-flex justify-content-between align-items-center ">

         <a class="navbar-brand" href="<?= BASEURL; ?>"><img src="<?= BASEURL ?>/img/logo-alnasar.svg" alt=""></a>
         <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarcuy" aria-controls="navbarcuy" aria-expanded="false" aria-label="Toggle navigation">
             <span class="navbar-toggler-icon"></span>
         </button>

         <div class="collapse navbar-collapse" id="navbarcuy">
             <ul class="navbar-nav me-auto mb-2 mb-lg-0 justify-content-center w-100">
                 <li class="nav-item">
                     <a class="nav-link <?= isset($data['home-active']) ? 'text-active' : '' ?>" aria-current="page" href="<?= BASEURL; ?>">Home</a>
                 </li>
                 <li class="nav-item">
                     <a class="nav-link <?= isset($data['about-active']) ? 'text-active' : '' ?>" href="<?= BASEURL; ?>/about">About</a>
                 </li>
                 <li class="nav-item">
                     <a class="nav-link <?= isset($data['mahasiswa-active']) ? 'text-active' : '' ?>" aria-current="page" href="<?= BASEURL; ?>/mahasiswa">Siswa</a>
                 </li>
                 <?php if ($_SESSION['role'] == 'admin') : ?>
                     <li class="nav-item">
                         <a class="nav-link <?= isset($data['admin-active']) ? 'text-active' : '' ?>" aria-current="page" href="<?= BASEURL; ?>/admin">Admin</a>
                     </li>
                 <?php endif; ?>
                 <li class="nav-item">
                     <a class="nav-link <?= isset($data['blog-active']) ? 'text-active' : '' ?>" aria-current="page" href="<?= BASEURL; ?>/blog">Blog</a>
                 </li>
             </ul>
             <?php if (!isset($_SESSION['login'])) : ?>
                 <a href="<?= BASEURL; ?>/auth/login" class="btn bg-active px-4 py-2 d-inline-block d-md-none">
                     <i class="fa fa-user me-2"></i>Login
                 </a>
             <?php else : ?>
                 <a href="<?= BASEURL; ?>/auth/logout" onclick="return confirm('Yakin logout?')" class="btn bg-active px-4 py-2 d-inline-block d-md-none">
                     <i class="fa-solid fa-right-from-bracket me-2"></i>Logout
                 </a>
             <?php endif; ?>
         </div>
         <?php if (!isset($_SESSION['login'])) : ?>
             <a href="<?= BASEURL; ?>/auth/login" class="btn bg-active px-4 py-2 d-none d-md-inline-block">
                 <i class="fa fa-user me-2"></i>Login
             </a>
         <?php else : ?>
             <a href="<?= BASEURL; ?>/auth/logout" onclick="return confirm('Yakin logout?')" class="btn bg-active px-4 py-2 d-none d-md-inline-block">
                 <i class="fa-solid fa-right-from-bracket me-2"></i>Logout
             </a>
         <?php endif; ?>

     </div>
 </nav>