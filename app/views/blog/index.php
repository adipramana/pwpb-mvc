<?php

$deskripsi = htmlspecialchars($data['posts'][0]['deskripsi'], ENT_QUOTES, 'UTF-8');

?>

<div class="container">
    <div class="row">
        <?php if ($data['posts']) : ?>
            <div class="col-lg-8 my-4">
                <img src="<?= BASEURL; ?>/img/<?= $data['posts'][0]['gambar'] ?>" alt="" class="img-fluid rounded" style="width: 100%;height: 20rem; object-fit: cover;">
                <div class="my-3 px-2 py-1 text-abu rounded-5" style="font-size: 12px; width: fit-content; border: 2px solid #FFC265;">
                    <?= $data['posts'][0]['kategori'] ?>
                </div>
                <h2 class="text-abu fw-bold mt-2"><?= $data['posts'][0]['judul'] ?></h2>
                <p class="text-abu" style="text-align: justify;"><?= Blog_model::limit($deskripsi, 180) ?></p>
                <a href="<?= BASEURL; ?>/blog/detail/<?= $data['posts']['0']['id_blog'] ?>" class="text-white text-decoration-none">
                    <button class="btn bg-active px-3 py-2 text-white">
                        Read more
                    </button>
                </a>
            </div>
        <?php else : ?>
            <div class="col-lg-8 my-4">
                <img src="https://i.pinimg.com/564x/78/54/bd/7854bdee9289e50694ea9e0ceb3c350d.jpg" alt="" class="img-fluid rounded" style="width: 100%;height: 20rem; object-fit: cover;">
                <div class="my-3 px-2 py-1 text-abu rounded-5" style="font-size: 12px; width: fit-content; border: 2px solid #FFC265;">
                    Lifestyle
                </div>
                <h2 class="text-abu fw-bold mt-2">7 Fakta Menarik tentang Zee JKT48 yang Mungkin Belum Anda Ketahui</h2>
                <p class="text-abu">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Neque aut doloribus libero magni nam voluptate.</p>
                <button class="btn bg-active px-3 py-2 text-white">
                    Read more
                </button>
            </div>
        <?php endif; ?>

        <div class="col-lg-4 my-4">
            <?php foreach ($data['posts'] as $index => $blog) : ?>
                <?php if ($index > 0) : ?>
                    <div class="d-flex gap-3 mb-3 align-items-center">
                        <img src="<?= BASEURL; ?>/img/<?= $blog['gambar'] ?>" class="rounded" alt="" style="width: 100px; height: 150px; object-fit: cover; ">
                        <div class="d-flex flex-column justify-content-around">
                            <p><?= $blog['kategori']; ?></p>
                            <h6><?= $blog['judul']; ?></h6>
                            <a href="<?= BASEURL; ?>/blog/detail/<?= $blog['id_blog'] ?>" class="text-light text-decoration-none">
                                <button class="btn bg-active text-white rounded-5 btn-sm" style="width: fit-content;">

                                    Read More
                                </button>
                            </a>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>
</div>