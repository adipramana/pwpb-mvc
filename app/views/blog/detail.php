<div class="container">
    <div class="row d-flex justify-content-center">
        <div class="col-sm-10 col-lg-10 my-5">
            <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb" class="mx-5">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= BASEURL; ?>/blog" class="text-decoration-none text-active"><i class="fa-solid fa-signs-post me-1"></i>Blog</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Detail Blog</li>
                </ol>
            </nav>
            <h1 class="mx-5 fw-bolder"><?= $data['post']['judul'] ?></h1>
            <div class="my-4 mx-5 d-flex justify-content-between align-items-center">
                <div class="d-flex gap-2 align-items-center">
                    <img src="<?= BASEURL; ?>/img/1697383364_zee.jpg" alt="" class="img-fluid rounded-5" style="width: 50px; height: 50px; object-fit: cover;">
                    <div>
                        <p class="mb-0 fw-bold">Adi Pramana</p>
                        <p class="mb-0 text-abu">28 Oktober 1945</p>
                    </div>
                </div>
                <div class="d-flex gap-1">
                    <a href="https://www.tiktok.com/@abcdemdjjdksnsks" target="_blank" class="btn bg-active text-white btn-lg rounded-circle">
                        <i class="fab fa-tiktok"></i>
                    </a>

                    <a href="https://www.linkedin.com/in/dekadii/" target="_blank" class="btn bg-active text-white btn-lg rounded-circle">
                        <i class="fab fa-linkedin"></i>
                    </a>

                    <a href="https://www.instagram.com/dekadii01" target="_blank" class="btn bg-active text-white btn-lg rounded-circle">
                        <i class="fab fa-instagram"></i>
                    </a>
                </div>
            </div>
            <img src="<?= BASEURL; ?>/img/<?= $data['post']['gambar'] ?>" alt="" class="img-fluid rounded" style="width: 100%; height: 25rem; object-fit: cover;">
            <div class="deskripsi my-4">
                <?= $data['post']['deskripsi'] ?>
            </div>
        </div>
    </div>
</div>