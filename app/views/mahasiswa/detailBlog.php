<div class="container py-5 d-flex justify-content-center">
    <div class="card" style="width: 100%;">
        <div class="card-body">
            <h5 class="card-title"><?= $data['post']['judul'] ?></h5>
            <h6 class="card-subtitle mb-2 text-body-secondary"><?= $data['post']['deskripsi'] ?></h6>
            <p class="card-text"><?= $data['post']['kategori'] ?></p>
            <p class="card-text"><?= $data['post']['gambar'] ?></p>
            <a href="<?= BASEURL; ?>/mahasiswa" class="card-link">Kembali</a>
        </div>
    </div>
</div>