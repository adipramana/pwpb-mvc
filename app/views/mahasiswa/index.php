<div class="container">
    <div class="row d-flex justify-content-center">
        <div class="col-lg-8">
            <?= Flasher::flash(); ?>
        </div>
    </div>

    <div class="row d-flex justify-content-center">
        <div class="col-md-6">
            <h3 class="text-center my-3">Data Siswa Skensa</h3>
            <?php foreach ($data['mhs'] as $mhs) : ?>
                <ul class="list-group mb-3">
                    <li class="list-group-item d-flex justify-content-between align-items-center"><?= $mhs['nama']; ?>
                        <div class="d-flex">
                            <a href="<?= BASEURL; ?>/mahasiswa/detail/<?= $mhs['nis'] ?>" class="badge bg-primary">detail</a>
                            <?php if ($_SESSION['role'] == 'admin') : ?>
                                <a href="<?= BASEURL; ?>/mahasiswa/hapus/<?= $mhs['nis'] ?>" class="badge bg-danger" onclick="return confirm('yakin hapus?')">hapus</a>
                            <?php endif; ?>
                        </div>
                    </li>
                    <li class="list-group-item"><?= $mhs['nis']; ?></li>
                    <li class="list-group-item"><?= $mhs['email']; ?></li>
                    <li class="list-group-item"><?= $mhs['jurusan']; ?></li>
                </ul>
            <?php endforeach; ?>
        </div>
        <div class="col-md-6">
            <h3 class="text-center my-3">Data Postingan Blog</h3>
            <?php foreach ($data['posts'] as $post) : ?>
                <ul class="list-group mb-3">
                    <li class="list-group-item d-flex justify-content-between align-items-center"><?= $post['judul']; ?>
                        <div class="d-flex">
                            <a href="<?= BASEURL; ?>/blog/detailBlog/<?= $post['id_blog'] ?>" class="badge bg-primary ">detail</a>
                            <?php if ($_SESSION['role'] == 'admin') : ?>
                                <a href="<?= BASEURL; ?>/blog/hapusBlog/<?= $post['id_blog'] ?>" class="badge bg-danger" onclick="return confirm('yakin hapus?')">hapus</a>
                            <?php endif; ?>
                        </div>
                    </li>
                    <li class="list-group-item"><?= Blog_model::limit($post['deskripsi'], 50) ?></li>
                    <li class="list-group-item"><?= $post['kategori']; ?></li>
                </ul>
            <?php endforeach; ?>
        </div>
    </div>
</div>