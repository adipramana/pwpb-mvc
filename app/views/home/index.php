<div class="container">
    <div class="row mt-5 d-flex justify-content-around">
        <div class="col-lg-6 mt-5 mb-5 mb-md-0">
            <button class="btn bg-green px-3 py-1 text-green rounded-5" style="width: fit-content;">
                No. 1 in Indonesia
            </button>
            <h1 class="fw-bold text-abu mt-4">
                Make it easy for your Umrah worship with Al Nasr Travel
            </h1>
            <p class="mt-2 text-abu">
                Facilitate your Umrah pilgrimage with us, we already have hundreds of thousands of customers, you can go for Umrah to Mecca and Medina
            </p>
            <button class="btn bg-active px-4 py-3" type="submit">Contact Us</button>
        </div>
        <div class="col-lg-6" style="width: fit-content;">
            <img src="<?= BASEURL ?>/img/img-hero.svg" alt="" class="img-fluid">
        </div>
    </div>
</div>