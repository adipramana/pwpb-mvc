<div style="height: 100vh;" class="d-flex justify-content-center align-items-center">
    <div class="container">
        <div class="row d-flex justify-content-between align-items-center">
            <div class="col-lg-5">
                <div class="card border-0 shadow-lg px-4 py-3">
                    <div class="card-body">
                        <?= Flasher::flash(); ?>
                        <h4 class="fw-bold mb-0">Register</h4>
                        <small class="text-secondary">Already have an account? <a href="<?= BASEURL; ?>/auth/login" class="text-active">Sign In</a> </small>
                        <form action="<?= BASEURL; ?>/auth/prosesRegister" class="my-4" method="post">
                            <div class="form-group mb-3">
                                <label for="" class="mb-2">Username</label>
                                <input type="text" class="form-control" placeholder="username" name="username" required>
                            </div>
                            <div class="form-group mb-3">
                                <label for="" class="mb-2">Email Adress</label>
                                <input type="email" class="form-control" placeholder="your@example.com" name="email" required>
                            </div>
                            <div class="form-group mb-3">
                                <label for="" class="mb-2">Password</label>
                                <input type="password" class="form-control" placeholder="Enter 6 character or more" name="password" required>
                            </div>
                            <div class="form-group mb-3">
                                <label for="" class="mb-2">Confirm Password</label>
                                <input type="password" class="form-control" placeholder="Confirm your password" name="confirm_password" required>
                            </div>
                            <button class="btn bg-active w-100 py-2 fw-bold text-white" type="submit">REGISTER</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-7 d-none d-md-flex justify-content-center">
                <img src="<?= BASEURL; ?>/img/auth/login.svg" alt="" class="w-75">
            </div>
        </div>
    </div>
</div>