<div style="height: 100vh;" class="d-flex justify-content-center align-items-center">
    <div class="container">

        <div class="row d-flex justify-content-center">
            <div class="col-lg-8">
                <?= Flasher::flash(); ?>
            </div>
        </div>

        <div class="row d-flex justify-content-between align-items-center">
            <div class="col-lg-5">
                <div class="card border-0 shadow-lg px-4 py-3">
                    <div class="card-body">
                        <h4 class="fw-bold mb-0">Login</h4>
                        <small class="text-secondary">Doesn't have an account yet? <a href="<?= BASEURL; ?>/auth/register" class="text-active">Sign Up</a> </small>
                        <form action="<?= BASEURL; ?>/auth/prosesLogin" class="my-4" method="post">
                            <div class="form-group mb-3">
                                <label for="" class="mb-2">Email Adress</label>
                                <input type="email" class="form-control" placeholder="your@example.com" name="email" required>
                            </div>
                            <div class="form-group mb-3">
                                <div class="d-flex justify-content-between mb-2">
                                    <label for="">Password</label>
                                    <a href="" class="text-active">Forgot Password?</a>
                                </div>
                                <input type="password" class="form-control" placeholder="Enter 6 character or more" name="password" required>
                            </div>
                            <div class="form-check my-4">
                                <input type="checkbox" class="form-check-input">
                                <label for="">Remember Me</label>
                            </div>
                            <button class="btn bg-active w-100 py-2 fw-bold text-white" type="submit">LOGIN</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-7 d-none d-md-flex justify-content-center">
                <img src="<?= BASEURL; ?>/img/auth/login.svg" alt="" class="w-75">
            </div>
        </div>
    </div>
</div>