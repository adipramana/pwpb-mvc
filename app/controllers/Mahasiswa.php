<?php

class Mahasiswa extends Controller
{

    public function __construct()
    {
        if (isset($_SESSION['login']) === false) {
            redirect('/auth/login');
        }
    }
    public function index()
    {
        $data['title'] = 'Siswa';
        $data['mhs'] = $this->model('Mahasiswa_model')->all();
        $data['posts'] = $this->model('Blog_model')->all();
        $data['mahasiswa-active'] = true;
        $this->view('templates/header', $data);
        $this->view('templates/navbar', $data);
        $this->view('mahasiswa/index', $data);
        $this->view('templates/footer');
    }

    public function detail($nis)
    {
        $data['title'] = 'Detail Siswa';
        $data['mhs'] = $this->model('Mahasiswa_model')->getSiswaByNis($nis);
        $data['mahasiswa-active'] = true;
        $this->view('templates/header', $data);
        $this->view('templates/navbar', $data);
        $this->view('mahasiswa/detail', $data);
        $this->view('templates/footer');
    }

    public function hapus($nis)
    {
        if ($this->model('Mahasiswa_model')->hapus($nis) > 0) {
            Flasher::setFlash('berhasil', 'dihapus', 'success');
            redirect('/mahasiswa');
        } else {
            Flasher::setFlash('gagal', 'dihapus', 'danger');
            redirect('/mahasiswa');
        }
    }
}
