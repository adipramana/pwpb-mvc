<?php

class Admin extends Controller
{
    public function __construct()
    {
        if (isset($_SESSION['login']) === false) {
            redirect('/auth/login');
        }

        if (isset($_SESSION['role']) === 'user') {
            redirect('/home');
        }
    }
    public function index()
    {
        $data['title'] = 'Admin';
        $data['admin-active'] = true;
        $this->view('templates/header', $data);
        $this->view('templates/navbar', $data);
        $this->view('admin/index', $data);
        $this->view('templates/footer');
    }
}
