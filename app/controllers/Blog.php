<?php

class Blog extends Controller
{
    public function __construct()
    {
        if (isset($_SESSION['login']) === false) {
            redirect('/auth/login');
        }
    }
    public function index()
    {
        $data['title'] = 'Blog';
        $data['posts'] = $this->model('Blog_model')->all();
        $data['blog-active'] = true;
        $data['nama'] = 'dekadi';
        $this->view('templates/header', $data);
        $this->view('templates/navbar', $data);
        $this->view('blog/index', $data);
        $this->view('templates/footer');
    }

    public function tambah()
    {
        if ($this->model('Blog_model')->tambahBlog($_POST) > 0) {
            Flasher::setFlash('berhasil', 'ditambahkan', 'success');
            header('Location: ' . BASEURL . '/admin');
        } else {
            Flasher::setFlash('gagal', 'ditambahkan', 'danger');
            header('Location: ' . BASEURL . '/admin');
        }
    }

    public function detail($id_blog)
    {
        $data['title'] = 'Detail Blog';
        $data['post'] = $this->model('Blog_model')->getBlogById($id_blog);
        $data['blog-active'] = true;
        $this->view('templates/header', $data);
        $this->view('templates/navbar', $data);
        $this->view('blog/detail', $data);
        $this->view('templates/footer');
    }

    public function detailBlog($id_blog)
    {
        $data['title'] = 'Detail Blog';
        $data['post'] = $this->model('Blog_model')->getBlogById($id_blog);
        $data['mahasiswa-active'] = true;
        $this->view('templates/header', $data);
        $this->view('templates/navbar', $data);
        $this->view('mahasiswa/detailBlog', $data);
        $this->view('templates/footer');
    }

    public function hapusBlog($id_blog)
    {
        if ($this->model('Blog_model')->hapusBlog($id_blog) > 0) {
            Flasher::setFlash('berhasil', 'dihapus', 'success');
            redirect('/mahasiswa');
        } else {
            Flasher::setFlash('gagal', 'dihapus', 'danger');
            redirect('/mahasiswa');
        }
    }
}
