<?php

class Auth extends Controller
{

    public function __construct()
    {
        if (isset($_SESSION['login']) === true) {
            redirect('/home');
        }
    }
    public function login()
    {
        $data['title'] = 'Login';
        $this->view('templates/header', $data);
        $this->view('auth/login');
        $this->view('templates/footer');
    }

    public function register()
    {
        $data['title'] = 'Register';
        $this->view('templates/header', $data);
        $this->view('auth/register');
        $this->view('templates/footer');
    }

    public function prosesRegister()
    {
        if ($this->model('User_model')->registerUser($_POST) > 0) {
            Flasher::setFlash('berhasil', 'register', 'success');
            redirect('/auth/login');
        } else {
            redirect('/auth/register');
        }
    }
    public function prosesLogin()
    {
        if ($this->model('User_model')->cekLogin($_POST) > 0) {
            redirect('/home');
        } else {
            redirect('/auth/login');
        }
    }

    public function logout()
    {
        session_unset();
        session_destroy();

        redirect('/auth/login');
        exit;
    }
}
