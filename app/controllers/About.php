<?php

class About extends Controller
{

    public function __construct()
    {
        if (isset($_SESSION['login']) === false) {
            redirect('/auth/login');
        }
    }
    public function index($nama = 'ulan', $pekerjaan = 'siswa', $umur = '12')
    {
        $data['nama'] = $nama;
        $data['pekerjaan'] = $pekerjaan;
        $data['umur'] = $umur;
        $data['title'] = 'About';
        $data['about-active'] = true;
        $this->view('templates/header', $data);
        $this->view('templates/navbar', $data);
        $this->view('about/index', $data);
        $this->view('templates/footer');
    }

    public function page()
    {
        $data['title'] = 'Page';
        $this->view('templates/header', $data);
        $this->view('about/page');
        $this->view('templates/footer');
    }
}
