<?php

class Home extends Controller
{

    public function __construct()
    {
        if (isset($_SESSION['login']) === false) {
            redirect('/auth/login');
        }
    }
    public function index()
    {
        $data['title'] = 'Home';
        $data['home-active'] = true;
        $data['models'] = $this->model('Mahasiswa_model')->all();
        $this->view('templates/header', $data);
        $this->view('templates/navbar', $data);
        $this->view('home/index', $data);
        $this->view('templates/footer');
    }
}
