<?php
class Mahasiswa_model
{
    private $table = 'siswa';
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function all()
    {
        $this->db->query('SELECT * FROM ' . $this->table);
        return $this->db->resultSet();
    }

    public function getSiswaByNis($nis)
    {
        $this->db->query('SELECT * FROM ' . $this->table . ' WHERE nis = :nis');
        $this->db->bind('nis', $nis);
        return $this->db->single();
    }

    public function hapus($nis)
    {
        $this->db->query("DELETE FROM " . $this->table . " WHERE nis = :nis");
        $this->db->bind('nis', $nis);
        $this->db->execute();

        return $this->db->rowCount();
    }
}
