<?php
class Blog_model
{
    private $table = 'blog';
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function all()
    {
        $this->db->query('SELECT * FROM ' . $this->table);
        return $this->db->resultSet();
    }

    public function getPostById($id_blog)
    {
        $this->db->query('SELECT * FROM ' . $this->table . ' WHERE id_blog = :id_blog');
        $this->db->bind('id_blog', $id_blog);
        return $this->db->single();
    }

    public function tambahBlog($data)
    {
        $judul = $data['judul'];
        $deskripsi = $data['deskripsi'];
        $kategori = $data['kategori'];

        $gambarName = $_FILES['gambar']['name'];
        $gambarTmpName = $_FILES['gambar']['tmp_name'];
        // $gambarSize = $_FILES['gambar']['size'];
        // $gambarType = $_FILES['gambar']['type'];
        
        $allowedExtensions = array("jpg", "jpeg", "png", "gif");
        $extension = pathinfo($gambarName, PATHINFO_EXTENSION);

        if (in_array($extension, $allowedExtensions)) {
            $uniqueFileName = time() . '_' . $gambarName;

            $uploadPath = '../public/img/' . $uniqueFileName;

            if (move_uploaded_file($gambarTmpName, $uploadPath)) {
                $query = "INSERT INTO blog (judul, deskripsi, kategori, gambar) VALUES (:judul, :deskripsi, :kategori, :gambar)";

                $this->db->query($query);
                $this->db->bind('judul', $judul);
                $this->db->bind('deskripsi', $deskripsi);
                $this->db->bind('kategori', $kategori);
                $this->db->bind('gambar', $uniqueFileName);

                $this->db->execute();

                return $this->db->rowCount();
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }

    public function getBlogById($id_blog)
    {
        $this->db->query('SELECT * FROM ' . $this->table . ' WHERE id_blog = :id_blog');
        $this->db->bind('id_blog', $id_blog);
        return $this->db->single();
    }

    public function hapusBlog($id_blog)
    {
        $query = "SELECT gambar FROM blog WHERE id_blog = :id_blog";
        $this->db->query($query);
        $this->db->bind('id_blog', $id_blog);
        $row = $this->db->single();
        $gambarToDelete = $row['gambar'];

        // Hapus data dari database
        $query = "DELETE FROM blog WHERE id_blog = :id_blog";
        $this->db->query($query);
        $this->db->bind('id_blog', $id_blog);
        $this->db->execute();

        // Hapus gambar dari direktori
        if (file_exists('../public/img/' . $gambarToDelete)) {
            unlink('../public/img/' . $gambarToDelete);
        } else {
            echo "gambar tidak ditemukan";
        }

        return $this->db->rowCount();
    }

    public static function limit($teks, $limit)
    {
        $deskripsi = $teks;
        $panjang = $limit;

        if (strlen($deskripsi) > $panjang) {
            $deskripsi_terpotong = substr($deskripsi, 0, $panjang) . '...';
        } else {
            $deskripsi_terpotong = $deskripsi;
        }

        echo $deskripsi_terpotong;
    }
}
