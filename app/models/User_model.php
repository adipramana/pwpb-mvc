<?php
class User_model
{
    private $table = "user";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function registerUser($data)
    {
        $username = $data["username"];
        $email = $data["email"];
        $password = $data["password"];
        $confirm_password = $data['confirm_password'];
        $hashedPassword = password_hash($password, PASSWORD_DEFAULT);

        $queryUser = "SELECT * FROM user WHERE email = :email";
        $this->db->query($queryUser);
        $this->db->bind("email", $email);
        $user = $this->db->single();

        if ($email == $user["email"]) {
            Flasher::setFlash('Gagal,', 'email sudah digunakan', 'danger');
            return -1;
        }

        if ($password !== $confirm_password) {
            Flasher::setFlash('tidak match', 'field password & confirm password ', 'danger');
            return -1;
        }

        $query = "INSERT INTO user (username, email, password, role) VALUES (:username, :email, :password, :role)";
        $this->db->query($query);
        $this->db->bind('username', $username);
        $this->db->bind('email', $email);
        $this->db->bind('password', $hashedPassword);
        $this->db->bind('role', 'user');
        $this->db->execute();

        return $this->db->rowCount();
    }

    public function cekLogin($data)
    {
        $email = $data['email'];
        $passwordPost = $data['password'];

        $query = "SELECT * FROM user WHERE email = :email";
        $this->db->query($query);
        $this->db->bind("email", $email);
        $user = $this->db->single();
        $count = $this->db->rowCount();

        if ($count > 0) {
            if (password_verify($passwordPost, $user["password"])) {
                $_SESSION['login'] = true;
                $_SESSION['role'] = $user['role'];
                return $this->db->rowCount();
            } else {
                Flasher::setFlash('Password salah,', 'coba kembali', 'danger');
                return -1;
            }
        } else {
            Flasher::setFlash('Email tidak ditemukan,', 'coba kembali', 'danger');
            return -1;
        }
    }
}
