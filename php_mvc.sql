-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 30, 2023 at 08:01 AM
-- Server version: 8.0.30
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `php_mvc`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id_blog` int NOT NULL,
  `judul` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `kategori` varchar(100) NOT NULL,
  `gambar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id_blog`, `judul`, `deskripsi`, `kategori`, `gambar`) VALUES
(9, '7 Fakta Menarik tentang Zee JKT48 yang Mungkin Belum Anda Ketahui', '<div>Apakah Anda penggemar berat Zee JKT48? Atau mungkin Anda baru saja mengenalnya? Dalam blog ini, kami akan mengungkap 7 fakta menarik tentang Zee JKT48 yang mungkin belum Anda ketahui. Temukan sisi unik dari member berbakat ini, dari latar belakangnya hingga prestasi di dunia musik idol Indonesia. Simaklah dan jadikan Zee sebagai inspirasi!<br><br><strong><br>Fakta 1: Lahir di Kota Kecil</strong><br>Zee JKT48 lahir di sebuah kota kecil yang mungkin jarang terdengar namanya. Namun, kota kecil inilah yang membentuk pribadinya dan memberikan rasa rendah hati yang melekat padanya. Temukan bagaimana kota asalnya memengaruhi perjalanan karirnya.<br><br></div><div><strong><br>Fakta 2: Audisi JKT48 yang Menentukan Takdirnya</strong><br>Belum banyak yang tahu bahwa Zee awalnya tidak berhasil dalam audisi pertamanya untuk JKT48. Namun, dengan tekad dan semangatnya, dia kembali mengikuti audisi dan berhasil. Bagaimana perjuangannya hingga akhirnya masuk ke dalam grup idol?<br><br></div><div><strong><br>Fakta 3: Multi-Talented Performer</strong><br>Zee bukan hanya seorang penyanyi idol, tapi juga seorang penari berbakat. Dia sering menampilkan aksi tari spektakuler di atas panggung. Temukan bagaimana kemampuan tari Zee mengesankan para penggemarnya.<br><br></div><div><strong><br>Fakta 4: Bahasa Inggris Lancar</strong><br>Zee memiliki kemampuan bahasa Inggris yang sangat baik. Ini membuatnya mudah berkomunikasi dengan penggemar internasional. Bagaimana dia mengasah kemampuan bahasa Inggrisnya?<br><br></div><div><strong><br>Fakta 5: Aktivitas Sosial</strong><br>Selain karir di JKT48, Zee juga dikenal sebagai sosok yang aktif dalam kegiatan sosial. Dia sering terlibat dalam kegiatan amal dan kegiatan masyarakat. Apa saja kontribusinya?<br><br></div><div><strong><br>Fakta 6: Kekuatan Suara Emas</strong><br>Suara Zee dikenal sebagai salah satu yang terbaik di JKT48. Temukan bagaimana dia mengasah vokalnya dan mencapai pencapaian suara emasnya.<br><br></div><div><strong><br>Fakta 7: Inspirasi untuk Para Pemuda dan Remaja</strong><br>Zee JKT48 adalah sosok yang bisa menjadi inspirasi bagi pemuda dan remaja di Indonesia. Bagaimana perjuangan dan dedikasinya dapat mengilhami generasi muda untuk mengejar impian mereka?<br><br></div><div><br>Dengan mengeksplorasi 7 fakta menarik ini, Anda akan mendapatkan wawasan lebih dalam tentang Zee JKT48 dan mengapresiasi perjalanan karir dan kepribadian yang luar biasa. Zee bukan hanya seorang idol, tetapi juga seorang inspirasi.</div>', 'Lifestyle', '1697383364_zee.jpg'),
(10, 'Cara Memasak Spaghetti Carbonara', '<div>Pelajari cara memasak spaghetti carbonara yang lezat dengan bahan-bahan sederhana dan langkah-langkah yang mudah diikuti. Spaghetti carbonara adalah hidangan klasik Italia yang terkenal dengan cita rasanya yang gurih dan krimi. Dalam blog ini, kami akan membahas resep langkah-demi-langkah untuk mempersiapkan hidangan ini di rumah Anda sendiri. Mulai dari memilih bahan yang tepat hingga teknik memasak yang benar, Anda akan mendapatkan panduan yang komprehensif. Kami juga akan berbicara tentang asal-usul hidangan ini dan beberapa variasi yang menarik. Apakah Anda seorang koki berpengalaman atau pemula, blog ini akan memberikan wawasan yang berguna tentang cara membuat spaghetti carbonara yang sempurna.</div>', 'Lifestyle', '1697388777_webcam-toy-photo77.jpg'),
(11, '10 Tips untuk Menjaga Kesehatan Mental Anda', '<div>Dapatkan wawasan tentang cara menjaga kesehatan mental Anda dengan 10 tips praktis yang dapat Anda terapkan sehari-hari. Kesehatan mental adalah aspek yang sangat penting dalam kehidupan kita, dan sering kali terabaikan. Dalam blog ini, kami akan berbicara tentang berbagai cara untuk merawat dan menjaga kesehatan mental Anda. Kami akan menjelaskan pentingnya perawatan diri, manajemen stres, tidur yang baik, dan berbagai teknik relaksasi. Kami juga akan membahas pentingnya mencari dukungan dan berbicara tentang perasaan Anda. Semua tips ini didasarkan pada penelitian ilmiah dan pengalaman pribadi, dan mereka dapat membantu Anda mencapai kesehatan mental yang lebih baik.</div>', 'Travel', '1697388842_webcam-toy-photo143.jpg'),
(12, 'Panduan Memulai Bisnis Online', '<div>Mulai bisnis online Anda dengan langkah-demi-langkah panduan yang komprehensif, termasuk strategi pemasaran, penjualan, dan manajemen. Bisnis online adalah salah satu peluang terbesar di era digital ini. Dalam blog ini, kami akan memberikan panduan lengkap untuk memulai bisnis online Anda sendiri. Kami akan membahas berbagai aspek, mulai dari pemilihan niche dan analisis pesaing hingga pembuatan situs web, pengembangan produk, dan strategi pemasaran. Anda akan belajar tentang pentingnya membangun merek Anda dan cara mengelola operasi harian bisnis Anda. Dengan bantuan panduan ini, Anda akan memiliki alat yang Anda butuhkan untuk meraih kesuksesan dalam bisnis online.</div>', 'Olahraga', '1697388906_WhatsApp Image 2022-09-14 at 13.56.20.jpg'),
(13, 'Destinasi Liburan Terbaik di Tahun 2023', '<div>Temukan destinasi liburan yang paling menarik dan populer untuk tahun 2023. Rencanakan perjalanan liburan Anda sekarang! Apakah Anda seorang penggemar petualangan yang mencari destinasi ekspedisi alam bebas atau Anda lebih suka berlibur santai di pantai, blog ini memiliki informasi yang Anda cari. Kami akan menyajikan daftar destinasi liburan yang akan menjadi tren di tahun 2023, termasuk tempat-tempat yang belum banyak diketahui. Anda akan menemukan saran untuk mengatur perjalanan Anda, tips berwisata, dan informasi tentang tempat-tempat wisata terkenal serta peristiwa khusus yang terjadi di berbagai lokasi. Bersiaplah untuk petualangan liburan terbaik dalam hidup Anda!</div>', 'Teknologi', '1697389173_webcam-toy-photo141.jpg'),
(15, 'Blog pertama', '<div>fff</div>', 'Teknologi', '1697416303_vinz-sama.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `nama` varchar(100) NOT NULL,
  `nis` int NOT NULL,
  `email` varchar(100) NOT NULL,
  `jurusan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(72) NOT NULL,
  `role` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `email`, `password`, `role`) VALUES
(1, 'adi', 'adi@gmail.com', '$2y$10$yWcML.m7Zkb/BbsTELlNpOpycBJ38k6UiPV/dinVTrJDueFFmX.UC', 'user'),
(2, 'dekadi', 'dekadi@gmail.com', '$2y$10$pg6gkI3SWWYNnZoIpE5vMejIBaaTABAtED95fV5PvEWyGSG5V5tgC', 'user'),
(3, 'admin', 'admin@gmail.com', '$2y$10$fH3Xy2KX7KnysN/XH5agLuW93TQFK6Pziauz.Q2Tbp4rE9V2N6cV.', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id_blog`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`nis`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id_blog` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `nis` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29891;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
